//
//  AboutViewController.swift
//  ListApp
//
//  Created by Solomon on 29/07/2019.
//  Copyright © 2019 Leapfroggr. All rights reserved.
//

import Foundation
import UIKit

class AboutViewController : UIViewController {
    
    var backButton : UIButton!
    
    override func loadView() {
        super.loadView()
        
        let back = UIButton()
        back.setTitle("‹ Go Back", for: .normal)
        back.setTitleColor(self.view.tintColor, for: .normal)
        self.backButton = back
        
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 20, weight: .bold)
        label.text = "About"
        
        let content = UILabel()
        content.text = "LeapFroggr (https://www.leapfroggr.com) is a software & mobile app company that provides the following services: IT consulting, web app development, iOS & Android mobile app development, mobile app marketing and IT staffing for medium and large scale businesses."
        content.numberOfLines = 0
        content.lineBreakMode = .byWordWrapping
        
        view.addSubview(back)
        view.addSubview(label)
        view.addSubview(content)
        
        back.translatesAutoresizingMaskIntoConstraints = false
        label.translatesAutoresizingMaskIntoConstraints = false
        content.translatesAutoresizingMaskIntoConstraints = false
        
        view.addConstraints([
            
            NSLayoutConstraint.init(item: back, attribute: .top, relatedBy: .equal, toItem: view.safeAreaLayoutGuide, attribute: .top, multiplier: 1, constant: 20),
            NSLayoutConstraint.init(item: back, attribute: .left, relatedBy: .equal, toItem: view, attribute: .left, multiplier: 1, constant: 20),
            
            NSLayoutConstraint.init(item: label, attribute: .top, relatedBy: .equal, toItem: back, attribute: .bottom, multiplier: 1, constant: 20),
            NSLayoutConstraint.init(item: label, attribute: .left, relatedBy: .equal, toItem: view, attribute: .left, multiplier: 1, constant: 20),
            
            NSLayoutConstraint.init(item: content, attribute: .top, relatedBy: .equal, toItem: label, attribute: .bottom, multiplier: 1, constant: 10),
            NSLayoutConstraint.init(item: content, attribute: .left, relatedBy: .equal, toItem: label, attribute: .left, multiplier: 1, constant: 0),
            NSLayoutConstraint.init(item: content, attribute: .right, relatedBy: .equal, toItem: view, attribute: .right, multiplier: 1, constant: -20)
            ])

    }
    
    
    override func viewDidLoad() {
        
        backButton.addTarget(self, action: #selector(dismissScreen), for: .touchUpInside)
        
    }
    
    @objc func dismissScreen() {
        // Dismisses the view controller that was presented modally by the view controller.
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func showScreen() {
        // Presents a view controller modally.
        let viewController = UIViewController()
        viewController.view.backgroundColor = .white
        self.present(viewController, animated: true, completion: nil)
    }
}
