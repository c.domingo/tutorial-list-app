//
//  EditorViewController.swift
//  ListApp
//
//  Created by Solomon on 29/07/2019.
//  Copyright © 2019 Leapfroggr. All rights reserved.
//

import Foundation
import UIKit

protocol EditorViewControllerDelegate : class {
    func editorSave(index: Int?, data: String)
}

class EditorViewController : UIViewController {
    
    var index : Int? = nil
    var data : String = ""
    
    var textbox = UITextField()
    var addButton = UIButton()
    
    weak var delegate : EditorViewControllerDelegate?
    
    override func loadView() {
        super.loadView()
        
        view.backgroundColor = UIColor.white
        
        textbox.borderStyle = .line
        
        addButton.backgroundColor = view.tintColor
        addButton.setTitle("SAVE", for: .normal)
        addButton.setTitleColor(.white, for: .normal)
        addButton.layer.cornerRadius = 4
        
        view.addSubview(textbox)
        view.addSubview(addButton)
        
        textbox.translatesAutoresizingMaskIntoConstraints = false
        addButton.translatesAutoresizingMaskIntoConstraints = false
        
        view.addConstraints([
            NSLayoutConstraint(item: textbox, attribute: .top, relatedBy: .equal, toItem: view.safeAreaLayoutGuide, attribute: .top, multiplier: 1, constant: 50),
            NSLayoutConstraint(item: textbox, attribute: .left, relatedBy: .equal, toItem: view.safeAreaLayoutGuide, attribute: .left, multiplier: 1, constant: 20),
            NSLayoutConstraint(item: textbox, attribute: .right, relatedBy: .equal, toItem: view.safeAreaLayoutGuide, attribute: .right, multiplier: 1, constant: -20),
            
            
            NSLayoutConstraint(item: textbox, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 50),
            
            NSLayoutConstraint(item: addButton, attribute: .top, relatedBy: .equal, toItem: textbox, attribute: .bottom, multiplier: 1, constant: 10),
            NSLayoutConstraint(item: addButton, attribute: .left, relatedBy: .equal, toItem: view.safeAreaLayoutGuide, attribute: .left, multiplier: 1, constant: 20),
            NSLayoutConstraint(item: addButton, attribute: .right, relatedBy: .equal, toItem: view.safeAreaLayoutGuide, attribute: .right, multiplier: 1, constant: -20),
            
            NSLayoutConstraint(item: addButton, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 50)
            
            ])
    }
    
    override func viewDidLoad() {
        
        self.textbox.text = self.data
        
        addButton.addTarget(self, action: #selector(addButtonClicked), for: .touchUpInside)
    }
    
    @objc func addButtonClicked() {
        let text = textbox.text ?? ""
        
        self.delegate?.editorSave(index: self.index, data: text)
    }
}
