//
//  ListCell.swift
//  ListApp
//
//  Created by Solomon on 30/07/2019.
//  Copyright © 2019 Leapfroggr. All rights reserved.
//

import Foundation
import UIKit

protocol ListCellDelegate : class {
    func listCellChecked(index: Int)
}

class ListCell : UITableViewCell {
    
    var index : Int?
    weak var delegate: ListCellDelegate?
    
    var check = UIButton()
    var label = UILabel()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setup() {
        super.awakeFromNib()
        self.addSubview(check)
        self.addSubview(label)
        
        check.setTitle("✔️", for: .normal)
        check.setTitleColor(self.tintColor, for: .normal)
        
        check.translatesAutoresizingMaskIntoConstraints = false
        label.translatesAutoresizingMaskIntoConstraints = false
        
        addConstraints([
            NSLayoutConstraint(item: check, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: check, attribute: .left, relatedBy: .equal, toItem: self, attribute: .left, multiplier: 1, constant: 20),
            NSLayoutConstraint(item: check, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 40),
            
            NSLayoutConstraint(item: label, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: label, attribute: .left, relatedBy: .equal, toItem: check, attribute: .right, multiplier: 1, constant: 20),
            NSLayoutConstraint(item: label, attribute: .right, relatedBy: .equal, toItem: self, attribute: .right, multiplier: 1, constant: -20)
            ])
        
        check.addTarget(self, action: #selector(checked), for: .touchUpInside)
    }
    
    @objc func checked() {
        if let index = index {
            self.delegate?.listCellChecked(index: index)
        }
    }
}
