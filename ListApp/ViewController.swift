//
//  ViewController.swift
//  ListApp
//
//  Created by Solomon on 29/07/2019.
//  Copyright © 2019 Leapfroggr. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var addButton: UIButton!
    
    var data = [ "one" ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        table.dataSource = self
        table.register(ListCell.self, forCellReuseIdentifier: "cell")
        
        table.delegate = self
        
        
        addButton.addTarget(self, action: #selector(showEditor), for: .touchUpInside)
    }
    
    @objc func showEditor() {
        let editor = EditorViewController()
        editor.delegate = self
        self.present(editor, animated: true, completion: nil)
    }
    
    func goEditor(index: Int, item: String) {
        let editor = EditorViewController()
        editor.index = index
        editor.data = item
        editor.delegate = self
        self.present(editor, animated: true, completion: nil)
    }

}

extension ViewController : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = table.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        let listcell = cell as? ListCell
        
        listcell?.label.text = self.data[indexPath.row]
        listcell?.index = indexPath.row
        listcell?.delegate = self
        
        return cell
    }
    
}

extension ViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        goEditor(index: indexPath.row, item: self.data[indexPath.row])
    }
}


extension ViewController : EditorViewControllerDelegate {
    
    func editorSave(index: Int?, data: String) {
        if let row = index {
            self.data[row] = data
        } else {
            self.data.append(data)
        }
        
        self.table.reloadData()
        self.dismiss(animated: true, completion: nil)
    }
}

extension ViewController : ListCellDelegate {
    
    func listCellChecked(index: Int) {
        self.data.remove(at: index)
        self.table.reloadData()
    }
}
